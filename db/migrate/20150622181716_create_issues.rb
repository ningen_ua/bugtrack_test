class CreateIssues < ActiveRecord::Migration
  def change
    create_table :issues do |t|
      t.string :title
      t.text :description
      t.integer :status
      t.string :code
      t.integer :priority
      t.belongs_to :user
      t.timestamps null: false
    end
    add_index :issues, :user_id

  end
end
